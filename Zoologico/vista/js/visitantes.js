var url = '../../controlador/fachada.php';

document.getElementById("Registrarvisitante").addEventListener('click', function () {
    Registrarvisitante();
})

document.getElementById("Consultarvisitante").addEventListener('click', function () {
    Consultarvisitante();
});

document.getElementById("modificarvisitante").addEventListener('click', function () {
    modificarvisitante();
});

function Consultarvisitante() {
    const data = new FormData();
    data.append('oper', 'Consultarvisitante');
    data.append('clase', 'VISITANTE');
    fetch(url, {
        method: 'POST', // or 'PUT'
        body: data,
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => {
            var html = "<tr><th> NOMBRES </th> <th> CEDULA </th> <th> CORREO </th> <th> EDAD </th> <th> DIRECCION </th> <th> CELULAR </th> </tr>";
            response.forEach(element => {
                html += "<tr><th>" + element.nombre_vis + " </th>" + "<th>" + element.cedula_vis + " </th>" + "<th>" + element.correo_usuario + " </th>"
                    + "<th>" + element.edad_vis + " </th>" + "<th>" + element.direccion_vis + " </th>" + "<th>" + element.celular_vis + " </th>  </tr>"
            }

            );
            document.getElementById("visitantes").innerHTML = (html);
        }
        );
}

function Registrarvisitante() {
    const data = new FormData();
    data.append('oper', 'Registrarvisitante');
    data.append('clase', 'VISITANTE');
    data.append('nombresS', document.getElementById("nombres").value);
    data.append('cedulaS', document.getElementById("cedula").value);
    data.append('correoS', document.getElementById("correo").value);
    data.append('edadS', document.getElementById("edad").value);
    data.append('direccionS', document.getElementById("direccion").value);
    data.append('celularS', document.getElementById("celular").value);
    fetch(url, {
        method: 'POST', // or 'PUT'
        body: data, // data can be `string` or {object}!

    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => alert(response));

}

function modificarvisitante() {
    let cedula = document.getElementById("ccabuscar");
    let evalormodificar = document.getElementById("nuevovalor");
    const data = new FormData();
        data.append('oper', 'modificarvisitante');
        data.append('clase', 'VISITANTE');
        data.append('ccabuscars', cedula.value);
        data.append('nuevovalors', evalormodificar.value);
    

    fetch(url, {
        method: 'POST', // or 'PUT'
        body: data, // data can be `string` or {object}!

    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => alert(response));
}