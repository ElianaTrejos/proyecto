var url = '../../controlador/fachada.php';

document.getElementById("Registrarencargado").addEventListener('click', function () {
    Registrarencargado();
})

document.getElementById("Consultarencargado").addEventListener('click', function () {
    Consultarencargado();
});

function Consultarencargado() {
    const data = new FormData();
        data.append('oper', 'Consultarencargado');
        data.append('clase', 'ENCARGADOS');

    fetch(url, {
        method: 'POST', // or 'PUT'
        body: data,
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => {
            var html = "<tr><th> NOMBRE </th> <th> CEDULA </th> <th> CELULAR </th> <th> DIRECCION </th> <th> CORREO </th> <th> CODIGOZONA </th> </tr>";
            response.forEach(element => {
                html += "<tr><th>" + element.nombre_enc + " </th>" + "<th>" + element.cedula_enc + " </th>" + "<th>" + element.celular_enc + " </th>"
                    + "<th>" + element.direccion_enc + " </th>" + "<th>" + element.correo_enc + " </th> " + "<th>" + element.codigo_zon + " </th>  </tr>"
            }

            );
            
            document.getElementById("ENCARGADOSS").innerHTML = (html);
        }
        );
}


function Registrarencargado() {
    const data = new FormData();
    data.append('oper', 'Registrarencargado');
    data.append('clase', 'ENCARGADOS');
    data.append('nombress', document.getElementById("nombres").value);
    data.append('celulars', document.getElementById("celular").value);
    data.append('cedulas', document.getElementById("cedula").value);
    data.append('correos', document.getElementById("correo").value);
    data.append('direccions', document.getElementById("direccion").value);
    data.append('codigozonas', document.getElementById("codigozona").value);

    fetch(url, {
        method: 'POST', // or 'PUT'
        body: data, // data can be `string` or {object}!

    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => alert(response));

}

document.getElementById("eliminarencargado").addEventListener('click', function () {
    eliminarencargado();
})

function eliminarencargado() {
    let codigo = document.getElementById("codigoeliminar");
    const data = new FormData();
        data.append('oper', 'eliminarencargado');
        data.append('clase', 'ENCARGADOS');
        data.append('codigoss', codigo.value);
    

    fetch(url, {
        method: 'POST', // or 'PUT'
        body: data, // data can be `string` or {object}!

    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => alert(response));
}