var url = '../../controlador/fachada.php';

document.getElementById("Registraranimal").addEventListener('click', function () {
    Registraranimal();
})

document.getElementById("Consultaranimal").addEventListener('click', function () {
    Consultaranimal();
});

function Consultaranimal() {
    let buscar = document.getElementById("idabuscar");
    const data = new FormData();
    if (buscar.value == '') {
        data.append('oper', 'Consultaranimal');
        data.append('clase', 'ANIMALES');
    } else {
        data.append('oper', 'consultarespecifico');
        data.append('clase', 'ANIMALES');
        data.append('idabuscar', buscar.value);
    }


    fetch(url, {
        method: 'POST', // or 'PUT'
        body: data,
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => {
            if(response == null){
                alert("IDENTIFICADOR -> "+idabuscar.value+" NO SE ENCUENTRA EN BASE DE DATOS");
            }else{
            var html = "<tr><th> NOMBRE </th> <th> IDENTIFICADOR </th> <th> COLOR </th> <th> CODIGOZONA </th> <th> PESO </th> <th> ALIMENTACION </th> <th> EDAD </th></tr>";
            response.forEach(element => {
                html += "<tr><th>" + element.nombre_ani + " </th>" + "<th>" + element.identificador_ani + " </th>" + "<th>" + element.color_ani + " </th>"
                    + "<th>" + element.codigo_zon + " </th>" + "<th>" + element.pesolibras_ani + " </th>" + "<th>" + element.clasificacion_ani + " </th>" + "<th>" + element.pesolibras_ani + " </th></tr>"
            }

            );
            document.getElementById("ANIMAL").innerHTML = (html);
        }
    }
        );
}

function Registraranimal() {
    const data = new FormData();
    data.append('oper', 'Registraranimal');
    data.append('clase', 'ANIMALES');
    data.append('nombre', document.getElementById("nombre").value);
    data.append('identificador', document.getElementById("identificador").value);
    data.append('color', document.getElementById("color").value);
    data.append('codigozona', document.getElementById("codigozona").value);
    data.append('peso', document.getElementById("peso").value);
    data.append('alimentacion', document.getElementById("alimentacion").value);
    data.append('edad', document.getElementById("edad").value);

    fetch(url, {
        method: 'POST', // or 'PUT'
        body: data, // data can be `string` or {object}!

    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => alert(response));

}
