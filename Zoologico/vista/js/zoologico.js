
var url = '../../controlador/fachada.php';

document.getElementById("registrarzoologico").addEventListener('click', function () {
    registrarzoologico();
})
/*
document.getElementById("consultarcita").addEventListener('click', function () {
    consultarcita();
});

function consultarcita() {
    let cconsul = document.getElementById("cedula");
    const data = new FormData();
    if (cconsul.value == '') {
        data.append('oper', 'consultarcita');
        data.append('clase', 'Citas');
    } else {
        data.append('oper', 'consultarcitas');
        data.append('clase', 'Citas');
        data.append('cedula', cconsul.value);
    }

    fetch(url, {
        method: 'POST', // or 'PUT'
        body: data,
    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => {
            var html = "<tr><th> Horario </th> <th> Codigo carro </th> <th> Cedula trabajador </th> <th> Codigo Servicio </th> <th> Descripcion Cita </th></tr>";
            response.forEach(element => {
                html += "<tr><th>" + element.horario_cita + " </th>" + "<th>" + element.codigo_carro + " </th>" + "<th>" + element.cedula_trabajador + " </th>"
                    + "<th>" + element.codigo_servicio + " </th>" + "<th>" + element.descripcion_cita + " </th> </tr>"
            }

            );
            document.getElementById("consultarcitas").innerHTML = (html);
        }
        );
}
*/

function registrarzoologico() {
    const data = new FormData();
    data.append('oper', 'registrarzoologico');
    data.append('clase', 'ZOOLOGICO');
    data.append('nombres', document.getElementById("nombre").value);
    data.append('nits', document.getElementById("nit").value);
    data.append('telefonos', document.getElementById("telefono").value);
    data.append('direccions', document.getElementById("direccion").value);
    data.append('correos', document.getElementById("correo").value);
    data.append('paginas', document.getElementById("pagina").value);

    fetch(url, {
        method: 'POST', // or 'PUT'
        body: data, // data can be `string` or {object}!

    }).then(res => res.json())
        .catch(error => console.error('Error:', error))
        .then(response => alert(response));

}