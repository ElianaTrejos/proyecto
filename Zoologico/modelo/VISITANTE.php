<?php 

class VISITANTE{

    function Registrarvisitante($param){
        extract($param);
       $sql = "INSERT INTO visitantes(
           nombre_vis, cedula_vis, edad_vis, direccion_vis, correo_usuario, celular_vis)
            VALUES (?, ?, ?, ?, ?, ?);";
        $rs = $conexion->getPDO()->prepare($sql);
        $conexion->getPDO()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                try {
                    $rs->execute(array($nombresS,$cedulaS, $edadS, $direccionS, $correoS, $celularS));
                      $state  = "se ha insertado el visitante  " .$nombresS;
                     
                    echo json_encode($state);
                 } catch (Exception $ex) {
                    //$state[0] = print_r($ex, 1);
                    $state = "Ocurrio error al insertar visitante " .$nombresS.$ex    ;
                    
                    echo json_encode($state);
                }   

    }

    function Consultarvisitante($param){
        extract($param);
        $sql = "select * from visitantes";
        $rs = $conexion->getPDO()->prepare($sql);
        if ($rs->execute(array())) {
            if ($filas = $rs->fetchAll(PDO::FETCH_ASSOC)) {
                foreach ($filas as $fila) {

                    $array[] = $fila;
                }
            }
        }
        echo json_encode(($array));

    }

    function modificarvisitante($param){
        extract($param);
        $sql = "UPDATE visitantes set nombre_vis='$nuevovalors' where cedula_vis= ?";
        $rs = $conexion->getPDO()->prepare($sql);
        if ($rs->execute(array($ccabuscars))) {
            if ($filas = $rs->fetchAll(PDO::FETCH_ASSOC)) {
                foreach ($filas as $fila) {

                    $array[] = $fila;
                }
            }
        }
        echo json_encode(($array));

    }
    
}