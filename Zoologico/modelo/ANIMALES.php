<?php 

class ANIMALES{

    function Registraranimal($param){
        extract($param);
       $sql = "INSERT INTO animales(
           nombre_ani, identificador_ani, codigo_zon, edad_ani, color_ani, clasificacion_ani, pesolibras_ani)
            VALUES (?, ?, ?, ?, ?, ?, ?);";
        $rs = $conexion->getPDO()->prepare($sql);
        $conexion->getPDO()->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
                try {
                    $rs->execute(array($nombre,$identificador, $codigozona, $edad, $color, $alimentacion, $peso));
                      $state  = "se ha insertado animal " .$nombre;
                     
                    echo json_encode($state);
                 } catch (Exception $ex) {
                    //$state[0] = print_r($ex, 1);
                    $state = "Ocurrio error al insertar Carro tipo " .$nombre.$ex    ;
                    
                    echo json_encode($state);
                }   

    }

    function Consultaranimal($param){
        extract($param);
        $sql = "select * from animales";
        $rs = $conexion->getPDO()->prepare($sql);
        if ($rs->execute(array())) {
            if ($filas = $rs->fetchAll(PDO::FETCH_ASSOC)) {
                foreach ($filas as $fila) {

                    $array[] = $fila;
                }
            }
        }
        echo json_encode(($array));

    }

    function consultarespecifico($param){
        extract($param);
        $sql = "select * from animales where identificador_ani= ?";
        $rs = $conexion->getPDO()->prepare($sql);
        if ($rs->execute(array($idabuscar))) {
            if ($filas = $rs->fetchAll(PDO::FETCH_ASSOC)) {
                foreach ($filas as $fila) {

                    $array[] = $fila;
                }
            }
        }
        echo json_encode(($array));

    }
    
}